#!/usr/bin/env python2.7

from __future__ import print_function
import itertools
import collections

class PeekableIterator(object):

    def __init__(self,iterator):
        self.itr = iterator
        self.deq = collections.deque()

    def from_itr(self):
        if not self.itr : return False

        if not self.deq or self.deq[-1]:
            try: 
                item = next( self.itr )
                self.deq.append( item if isinstance(item,list) else [item] )
            except StopIterator as SIExc:
                self.deq.append( [] )

        return bool( self.deq )

    def close(self,):
        i = None
        if self.itr :
            #i = itertools.chain ( iter(self.deq) , self.itr )
            i = itertools.chain(*(list(map(iter,self.deq)) + [self.itr]))
            self.itr = None
        return i

    def peek_n (self, n=1, erase=False):

        lst = []
        if n < 1: return lst

        while len(lst) < n:

            if not self.deq:
                self.from_itr()

            popped_lst = None

            if self.deq: 
                popped_lst = self.deq.popleft()
                lst.extend( popped_lst )

            if popped_lst == []: self.deq.appendleft( popped_lst )
            if len(lst) >= n or not(popped_lst): break

        restore_idx = 0 if not erase else n
        if len(lst) > restore_idx:
            self.deq.appendleft(lst[restore_idx:])

        return lst[:n]

if __name__ == '__main__':

    list_iterated = range(10,25)

    for m,n,p in  [ (0,1,2), (1,2,0), (1,0,1) ]:

        itr = PeekableIterator ( i for i in list_iterated )

        print ('peek {} non-destructively: {}\n'.format( m,itr.peek_n(m,)) if m > 0 else '', end = '')
        print ('peek {} destructively:     {}\n'.format( n,itr.peek_n(n,True)) if n > 0 else '', end = '')
        print ('peek {} non-destructively: {}\n'.format( p,itr.peek_n(p,)) if p > 0 else '', end = '')

        for k in itr.close(): print( k,end=' ')
        print( '\n-----------------')

